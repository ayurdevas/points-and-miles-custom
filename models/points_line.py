# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import models, fields


class PointsLine(models.Model):
    _inherit = 'points.line'

    invoice_line_id = fields.One2many(
        'account.invoice.line',
        'points_line_id',
        'Invoice Lines'
    )
