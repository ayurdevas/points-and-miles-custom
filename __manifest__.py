# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{

    'name': 'Points & miles Custom',

    'version': '1.0',

    'category': '',

    'summary': 'Points & miles',

    'author': 'Multidevas SA',

    'website': 'ayurdevas.com',

    'depends': [
        'points_miles',
        'points_from_invoices',
    ],

    'data': [
        'views/points_line_views.xml'
    ],

    'installable': True,

    'auto_install': False,

    'application': True,

    'description': 'Points & miles Custom',

}
